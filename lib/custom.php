<?php
/**
 * Custom functions
 */

/*
 * When using the embedded framework, use it only if the framework
 * plugin isn't activated.
 */
 
// Don't do anything when we're activating a plugin to prevent errors
// on redeclaring Titan classes
if ( ! empty( $_GET['action'] ) && ! empty( $_GET['plugin'] ) ) {
    if ( $_GET['action'] == 'activate' ) {
        return;
    }
}
// Check if the framework plugin is activated
$useEmbeddedFramework = true;
$activePlugins = get_option('active_plugins');
if ( is_array( $activePlugins ) ) {
    foreach ( $activePlugins as $plugin ) {
        if ( is_string( $plugin ) ) {
            if ( stripos( $plugin, '/titan-framework.php' ) !== false ) {
                $useEmbeddedFramework = false;
                break;
            }
        }
    }
}
// Use the embedded Titan Framework
if ( $useEmbeddedFramework ) {
    require_once( plugin_dir_path( __FILE__ ) . 'titan-framework/titan-framework.php' );
}

/*
 * Theme Options Page
 */

// titan framework init
$titan = TitanFramework::getInstance( 'momotion' );

// create menu item
$panel = $titan->createAdminPanel( array(
    'name' => 'Momotion Options',
) );

$panel->createOption( array(
    'type' => 'note',
    'desc' => 'Customize your Home Page and Theme Options here'
) );

// create options

// logo
$panel->createOption( array(
    'name' => 'Logo',
    'type' => 'heading',
) );

$panel->createOption( array(
    'name' => 'Logo',
    'id' => 'logo',
    'type' => 'upload',
    'desc' => 'Upload your logo here. Recommended images size: 300x65px'
) );

// slider
$panel->createOption( array(
    'name' => 'Slider',
    'type' => 'heading',
) );

$panel->createOption( array(
    'name' => 'Cyclone Slider ID',
    'id' => 'cyclone_slider_code',
    'type' => 'text',
    'desc' => 'Put your Cyclone Slider Slideshow ID here. Recommended images size: 1960x600px',
) );

// promo text
$panel->createOption( array(
    'name' => 'Promo Text',
    'type' => 'heading',
) );

$panel->createOption( array(
    'name' => 'Promo Heading',
    'id' => 'promo_heading',
    'type' => 'text',
    'desc' => 'Put your promo text heading here. Please, don\'t use h1-h6 tags here ',
) );

$panel->createOption( array(
    'name' => 'Promo Heading',
    'id' => 'promo_text',
    'type' => 'textarea',
    'desc' => 'Put your promo text here. You can use HTML tags',
) );

// round pages picker
$panel->createOption( array(
    'name' => 'Featured Pages',
    'type' => 'heading',
) );

$panel->createOption( array(
    'type' => 'note',
    'desc' => 'Select pages to display after the slider with round thumbnails.'
) );

$panel->createOption( array(
'name' => 'Select Page 1',
'id' => 'round_page_1',
'type' => 'select-pages',
'desc' => 'Select a page'
) );
$panel->createOption( array(
    'name' => 'Round Image 1',
    'id' => 'rimage_1',
    'type' => 'upload',
    'desc' => 'Upload your image. Recommended images size: 222x222px'
) );

$panel->createOption( array(
'name' => 'Select Page 2',
'id' => 'round_page_2',
'type' => 'select-pages',
'desc' => 'Select a page'
) );
$panel->createOption( array(
    'name' => 'Round Image 2',
    'id' => 'rimage_2',
    'type' => 'upload',
    'desc' => 'Upload your image. Recommended images size: 222x222px'
) );


$panel->createOption( array(
'name' => 'Select Page 3',
'id' => 'round_page_3',
'type' => 'select-pages',
'desc' => 'Select a page'
) );
$panel->createOption( array(
    'name' => 'Round Image 3',
    'id' => 'rimage_3',
    'type' => 'upload',
    'desc' => 'Upload your image. Recommended images size: 222x222px'
) );


$panel->createOption( array(
    'type' => 'note',
    'desc' => 'Select pages to display after first banner.'
) );

$panel->createOption( array(
'name' => 'Select Page 1',
'id' => 'page_1',
'type' => 'select-pages',
'desc' => 'Select a page'
) );

$panel->createOption( array(
'name' => 'Select Page 2',
'id' => 'page_2',
'type' => 'select-pages',
'desc' => 'Select a page'
) );

$panel->createOption( array(
'name' => 'Select Page 3',
'id' => 'page_3',
'type' => 'select-pages',
'desc' => 'Select a page'
) );

$panel->createOption( array(
'name' => 'Select Page 4',
'id' => 'page_4',
'type' => 'select-pages',
'desc' => 'Select a page'
) );

// images picker
$panel->createOption( array(
    'name' => 'Banners',
    'type' => 'heading',
) );

$panel->createOption( array(
    'type' => 'note',
    'desc' => 'Select images to display as banners on Home Page'
) );

$panel->createOption( array(
    'name' => 'Image 1',
    'id' => 'image_1',
    'type' => 'upload',
    'desc' => 'Upload your image. Recommended images size: 1960x400px'
) );

$panel->createOption( array(
    'name' => 'Image 2',
    'id' => 'image_2',
    'type' => 'upload',
    'desc' => 'Upload your image. Recommended images size: 1960x400px'
) );

// contact form ettings
$panel->createOption( array(
    'name' => 'Contacts',
    'type' => 'heading',
) );

$panel->createOption( array(
    'name' => 'Contacts Heading',
    'id' => 'contacts_heading',
    'type' => 'text',
    'desc' => 'Heading of Contacts block',
) );

$panel->createOption( array(
    'name' => 'Contacts Text',
    'id' => 'contacts_text',
    'type' => 'textarea',
    'desc' => 'Text of Contacts block',
) );

$panel->createOption( array(
    'name' => 'Contact Form 7 Shortcode',
    'id' => 'cf7_code',
    'type' => 'text',
    'desc' => 'Put your Contact Form 7 shortcode here',
) );

$panel->createOption( array(
    'name' => 'Phone Number',
    'id' => 'phone_number',
    'type' => 'text',
    'desc' => 'Enter phone number to display on footer',
) );

$panel->createOption( array(
    'name' => 'Address',
    'id' => 'address',
    'type' => 'text',
    'desc' => 'Enter your address',
) );

$panel->createOption( array(
    'name' => 'Email',
    'id' => 'email',
    'type' => 'text',
    'desc' => 'Enter your email',
) );


$panel->createOption( array(
    'name' => 'Facebook Link',
    'id' => 'fb_link',
    'type' => 'text',
    'desc' => 'Enter your Facebook Page URL to display on footer',
) );

$panel->createOption( array(
    'name' => 'Twitter Link',
    'id' => 'tw_link',
    'type' => 'text',
    'desc' => 'Enter your Twitter URL to display on footer',
) );

$panel->createOption( array(
    'name' => 'YouTube Link',
    'id' => 'yt_link',
    'type' => 'text',
    'desc' => 'Enter YouTube URL to display on footer',
) );

// code picker
$panel->createOption( array(
    'name' => 'Additional Code',
    'type' => 'heading',
) );

$panel->createOption( array(
    'name' => 'Additional Javascript',
    'id' => 'additional_javascript',
    'type' => 'code',
    'desc' => 'Put your additional javascript rules here',
    'lang' => 'javascript',
) );

$panel->createOption( array(
    'name' => 'Custom CSS',
    'id' => 'custom_css',
    'type' => 'code',
    'desc' => 'Put your custom CSS rules here',
    'lang' => 'css',
) );

//save options
$panel->createOption( array(
    'type' => 'save'
) );


/*
 * Additional Functions
 */

// excerpts for pages
add_action('init', 'momotion_page_init');
function momotion_page_init() {
	add_post_type_support( 'page', 'excerpt' );
}


// round image size
if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'round', 222, 222, true ); //200 pixels 
}

// responsive images auto class
function add_image_responsive_class($content) {
   global $post;
   $pattern ="/<img(.*?)class=\"(.*?)\"(.*?)>/i";
   $replacement = '<img$1class="$2 img-responsive"$3>';
   $content = preg_replace($pattern, $replacement, $content);
   return $content;
}
add_filter('the_content', 'add_image_responsive_class');
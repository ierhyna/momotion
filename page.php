<div class="row">
    <div class="col-lg-8 col-lg-offset-2 ">
        <?php
        if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
            the_post_thumbnail( 'full', array(
            'class' => "img-responsive",
            )
            );
        }
        ?>
        <?php get_template_part('templates/page', 'header'); ?>
        <?php get_template_part('templates/content', 'page'); ?>
    </div>
</div>

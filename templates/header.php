<header class="banner" role="banner">
    <div class="row">
        <div class="col-lg-12">
            <a class="brand" href="<?php echo home_url('/') ?>"><img src="<?php echo esc_attr($Logo); ?>" class="img-responsive" alt="Responsive image"></a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <nav class="nav-main" role="navigation">
                <?php
                if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav nav-pills'));
                endif;
                ?>
            </nav>
        </div>
    </div>
</header>

<?php
$titan = TitanFramework::getInstance('momotion');
$Logo = $titan->getOption('logo');
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        <div class="row banner" role="banner">
                <div class="col-md-4 col-md-offset-4 text-center">
                    <a class="brand" href="<?php echo home_url('/') ?>"><img src="<?php echo esc_attr($Logo); ?>" class="" alt=""></a>
                </div>
        </div>
        <header class="row navbar navbar-default navbar-static-top">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <nav class="collapse navbar-collapse" role="navigation">
                <?php
                if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav center'));
                endif;
                ?>
            </nav>
        </header>
        </div>
    </div>
</div>

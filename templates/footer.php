<?php
$titan = TitanFramework::getInstance('momotion');
$ContactsHeading = $titan->getOption('contacts_heading');
$ContactsText = $titan->getOption('contacts_text');
$ContactForm = $titan->getOption('cf7_code');
$Phone = $titan->getOption('phone_number');
$Address = $titan->getOption('address');
$Email = $titan->getOption('email');
$Facebook = $titan->getOption('fb_link');
$Twitter = $titan->getOption('tw_link');
$YouTube = $titan->getOption('yt_link');
?>
<div class="wrap container-fluid">
    <div class="row"><hr>
        <div class="col-md-12">
            <div class="container contact-info">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center">
                        <?php
                        echo '<h3>' . $ContactsHeading . '</h3>';
                        echo '<p>' . $ContactsText . '</p>';
                        ?>
                    </div>
                </div>
                <div class="row half-buffer">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                        <?php echo do_shortcode($ContactForm); ?>
                    </div>
                </div>
            </div>
            <div class="row half-buffer">
                <div class="col-md-12 footer-panel">
                    <div class="col-md-6 col-md-offset-3 text-center"> 
                        <?php
                        echo '<p class="phone">' . $Phone . '</p>';
                        echo '<p>' . $Address . '</p>';
                        ?>
                    </div>
                </div>
            </div>
            <footer class="content-info text-center" role="contentinfo">
                <a href="<?php echo $Facebook; ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb.png" class="" alt="FaceBook"></a>
<!--                <a href="<?php echo $Twitter; ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/tw.png" class="" alt="Twitter"></a>
                <a href="<?php echo $YouTube; ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/yt.png" class="" alt="YouTube"></a>-->
                <?php echo '<br><br><p class="email">' . $Email . '</p>'; ?>
                <p class="copy">&copy; Copyright <?php bloginfo('name'); ?> <?php echo date('Y'); ?></p>
                <p class="designed">Designed by Vladimir Gulian</p>

                <div class="fb-like" data-layout="button_count" data-action="like" data-show-faces="true" data-share="false"></div><p></p>
            </footer>
        </div>
    </div>
</div>
<?php wp_footer(); ?>

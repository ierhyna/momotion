<?php
// get theme options 
$titan = TitanFramework::getInstance('momotion');
$CycloneSlider = $titan->getOption('cyclone_slider_code');
$PromoHeading = $titan->getOption('promo_heading');
$PromoText = $titan->getOption('promo_text');
$RoundPage1 = $titan->getOption('round_page_1');
$RImage1 = $titan->getOption('rimage_1');
$RoundPage2 = $titan->getOption('round_page_2');
$RImage2 = $titan->getOption('rimage_2');
$RoundPage3 = $titan->getOption('round_page_3');
$RImage3 = $titan->getOption('rimage_3');
$Page1 = $titan->getOption('page_1');
$Page2 = $titan->getOption('page_2');
$Page3 = $titan->getOption('page_3');
$Page4 = $titan->getOption('page_4');
$Image1 = $titan->getOption('image_1');
$Image2 = $titan->getOption('image_2');
?>
<div class="row buffer">
    <!--    <div class="col-md-12">-->
    <?php
    // get slider
    if (function_exists('cyclone_slider'))
        cyclone_slider($CycloneSlider);
    ?>
    <!--    </div>-->
</div>

<div class="row buffer promo">
    <div class="container">
        <div class="col-md-8 col-md-offset-2 text-center half-buffer">
            <?php
            echo '<h2 class="heading-promo">' . $PromoHeading . '</h2>';
            echo '<p>' . $PromoText . '</p>';
            ?>
        </div>
    </div>
</div>
<div class="row half-buffer text-center">
    <div class="col-centered col-lg-11 full-height-img">
        <div class="col-md-4 home-page">
            <?php
            /*
             * Round Page 1
             */
            // get page
            $GetRoundPage1 = get_post($RoundPage1);
            // get thumbs
            //$thumb1 = get_the_post_thumbnail($RoundPage1, 'round', array('class' => 'img-circle'));
            // echo page
            //echo $thumb1;
            ?>
            <img src="<?php echo esc_attr($RImage1); ?>" class="img-circle" alt="" width="222" height="222">
            <?php
            echo '<h3>' . apply_filters('the_title', $GetRoundPage1->post_title) . '</h3>';
            echo apply_filters('the_excerpt', $GetRoundPage1->post_excerpt);
            ?>
            <a type="button" class="btn btn-default btn-lg" href="<?php echo get_permalink($RoundPage1); ?>">Ещё</a>
        </div>
        <div class="col-md-4 home-page">
            <?php
            /*
             * Round Page 2
             */
            // get page
            $GetRoundPage2 = get_post($RoundPage2);
            // get thumbs
            //$thumb2 = get_the_post_thumbnail($RoundPage2, 'round', array('class' => 'img-circle'));
            // echo page
            //echo $thumb2;
            ?>
            <img src="<?php echo esc_attr($RImage2); ?>" class="img-circle" alt="" width="222" height="222">
            <?php
            echo '<h3>' . apply_filters('the_title', $GetRoundPage2->post_title) . '</h3>';
            echo apply_filters('the_excerpt', $GetRoundPage2->post_excerpt);
            ?>
            <a type="button" class="btn btn-default btn-lg" href="<?php echo get_permalink($RoundPage2); ?>">Ещё</a>
        </div>
        <div class="col-md-4 home-page">
            <?php
            /*
             * Round Page 3
             */
            // get page
            $GetRoundPage3 = get_post($RoundPage3);
            // get thumbs
            //$thumb3 = get_the_post_thumbnail($RoundPage3, 'round', array('class' => 'img-circle'));
            // echo page
            //echo $thumb3;
            ?>
            <img src="<?php echo esc_attr($RImage3); ?>" class="img-circle" alt="" width="222" height="222">
            <?php
            echo '<h3>' . apply_filters('the_title', $GetRoundPage3->post_title) . '</h3>';
            echo apply_filters('the_excerpt', $GetRoundPage3->post_excerpt);
            ?>
            <a type="button" class="btn btn-default btn-lg" href="<?php echo get_permalink($RoundPage3); ?>">Ещё</a>
        </div>
    </div>
</div>
<div class="row half-buffer">
    <!--    <div class="col-md-12">-->
    <img src="<?php echo esc_attr($Image1); ?>" class="img-responsive" alt="Responsive image">
    <!--    </div>-->
</div>
<div class="row buffer text-center">
    <div class="col-centered col-lg-11 full-height">
        <div class="col-md-3 home-page">
            <?php
            /*
             * Page 1
             */
            // get page
            $GetPage1 = get_post($Page1);
            // echo page
            echo '<h3>' . apply_filters('the_title', $GetPage1->post_title) . '</h3>';
            echo apply_filters('the_excerpt', $GetPage1->post_excerpt);
            ?>
            <a type="button" class="btn btn-default btn-lg" href="<?php echo get_permalink($Page1); ?>">Ещё</a>
        </div>
        <div class="col-md-3 home-page">
            <?php
            /*
             * Page 2
             */
            // get page
            $GetPage2 = get_post($Page2);
            // echo page
            echo '<h3>' . apply_filters('the_title', $GetPage2->post_title) . '</h3>';
            echo apply_filters('the_excerpt', $GetPage2->post_excerpt);
            ?>
            <a type="button" class="btn btn-default btn-lg" href="<?php echo get_permalink($Page2); ?>">Ещё</a>
        </div>
        <div class="col-md-3 home-page">
            <?php
            /*
             * Page 3
             */
            // get page
            $GetPage3 = get_post($Page3);
            // echo page
            echo '<h3>' . apply_filters('the_title', $GetPage3->post_title) . '</h3>';
            echo apply_filters('the_excerpt', $GetPage3->post_excerpt);
            ?>
            <a type="button" class="btn btn-default btn-lg" href="<?php echo get_permalink($Page3); ?>">Ещё</a>
        </div>
        <div class="col-md-3 home-page">
            <?php
            /*
             * Page 4
             */
            // get page
            $GetPage4 = get_post($Page4);
            // echo page
            echo '<h3>' . apply_filters('the_title', $GetPage4->post_title) . '</h3>';
            echo apply_filters('the_excerpt', $GetPage4->post_excerpt);
            ?>
            <a type="button" class="btn btn-default btn-lg" href="<?php echo get_permalink($Page4); ?>">Ещё</a>
        </div>
    </div>
</div>
<div class="row half-buffer">
    <!--    <div class="col-md-12">-->
    <img src="<?php echo esc_attr($Image2); ?>" class="img-responsive" alt="Responsive image">
    <!--    </div>-->
</div>
